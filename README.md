# haskell adventure 

attempt to explain my recent haskell adventure
-----------------------------------

so I have trying to bring in haskell in my daily work,  turns out it's not easy.
I still don't feel that ease that I feel with other "easier" languages but the
situation is way better than it was before my last attempt. the haskell community
is nice and welcoming,  and during this little haskell adventure I got to interact
with some of great folks there.


problem statement:  compare two sorted files :)

well, not exactly, files had format
`inventory_id1\titem_id1,count1 item_id2,count2 ...`

now,  the idea is pretty simple sort both files that are to be compared.
use a two pointer approach to find `inventory_id` that are present in both,
present in only one of them.  For storing `item_id`, using map seemed good
enough at beginning (later tested with sorting them and again 2 pointer but,
map gave better temporal perforance [not thoroughly tested though])


my intial implementation was in Golang which I have come to like given it's KISS
philosophy,  it's a modern language with simpler construct which works great for
my tiny brain. If still not sold just have a look at google result for this language,
it has grown like a virus and that's a given as Google is investing on it heavily &
lot of killer applications exists showing strength and weakness.

so golang implementation was easy to come up with,  and it worked fine.
taking roughly 2 seconds for file with 10k inventory items. (~35MB data) 

The actual data was 800 times bigger, so roughly 1400 seconds, ~23 mins, and since
this was a batch job,  I didn't bothered investing any further.

BUT, being who I am,  I had to find some thing to have "fun",  so I thought lets
write this in haskell,  satisfying my haskell itch and as I had some time.

so I wrote my haskell implementation, the first draft took just 35x more time.

so I googled how to improve haskell performance, and didn't find much except, "you need to tame lazyness"

eventually I stumbled on Real World Haskell's chapter on performance tuning, and so I played with <>.

made lot of changes such as:
- moved from default String to Text (lazy for streaming)
- then moved to bytestring from text
- moving to strict map implementation for IntMap
- IntMap to Map with ByteString as key.
- made functions tail recursive
- language extenstion Strict
- inilining all functions

eventually after lot of hit and trial, re-implementations(about 9 different combinations),
got haskell code to reach 5x time of golang implementation.
Now I was feeling so dejected,  even after 10 hours of dedicated effort, still
goal of reaching performance comparable to golang was so far.


So I did,  what we do when we have no idea what to do.  I asked on stack overflow.
even after first 2 days no great input.  Then I tried asking same question at
haskell's sub-reddit.  To be honest I was not sure, if that right thing but thought
even if nothing happens well atleast I tried.
link to post : https://www.reddit.com/r/haskell/comments/odfpoe/looking_for_ways_to_improve_performance_of/

After 2 hours, I got first comment for my post from user by name @noughtmare,
saying to move to better parser that does parsing in single pass and to use 
IntMap instead of Map.

To be honest I had no idea what he meant with single pass parser, and my experience
with IntMap showed Map to be better.

and then after 4 hours, he re-wrote my golang implementation following his own
suggestions into haskell,
and that code was fast. just marginally slower than golang implementation.
and I was elated, the code was pretty nice and it used a custom parser with all typeclasses
that comes to mind when anyone hear haskell. 

my years of trying to improve my haskell understanding meant I knew those
concept but didn't had much exposure to them due lack of dedicated practise.
It took me some time to grook code, and then I profiled the code and to my sadness, the
memory consumption was linearly growing with input size, and this was a red flag to me, as
I wanted to complete this task with minimal RAM.

so I moved from Strict bytestring to lazy bytestring

which I have been using to get free streaming.

but this didn't changed the memory consumption.
my heart shunked as I didn't knew how to fix this behaviour, but I was so close to finishing line.

profiling revealed IntMap objects were the cause, but code was using strict IntMap.
so I did what I do, when I have no idea what's happening,  hit and trial.

putting strict language extension, further slowed code. 
putting inline pragma, too slowed.
data structures were already decorated with UNWRAP and bangs where appropriate.

interestingly I saw, the monadic and applicative instance were strict, and this was
first time I had seen this.  I knew I needed lazy file reading to tap memory growth
and since these instance were in a way iterating over file. so maybe they shouldn't
be really strict. dunno, so I just tried removing bangs from applicative and monadic
definitions, and ran the code.

And to my great surprise I had this beautiful, memory graph where the program never consumed more than 5 MB in entire duration. this further improve timing of program making it run within 80% of golang implementation's.

Now I was elated, I had found a way to improve code which was written by some one
better than me.


LESSON LEARNT

- golang is pretty easy, and it's way difficult to screw yourself with it
- haskell is powerful, and great but in hands of master


learnt profiling in haskell, it's actually pretty easy and helps to find problems
knew where I had to invest my time, to improve further.

read about applicative parsing
read about CAS

